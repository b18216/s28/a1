/*
	Activity:

	>> Using Robo3T, repopulate our users collections using CRUD operations (Same database: session28)

//Create
	>> Add at least five of the members of your favorite fictional group or musical band into our users collection with the following fields:

		-firstName -string
		-lastName -string
		-email -string
		-password -string
		-isAdmin -boolean 
			note: initially none of the users are admin.

	db.users.insertMany([
		{
			firstName : "Ely",
			lastName : "Buendia",
			email: "e.buendia@gmail.com",
			password: "heads4life",
			isAdmin: false,
			
		},
		{
			firstName : "Raymond",
			lastName : "Marasigan",
			email: "ray.marasigan@gmail.com",
			password: "ray2mrsgn",
			isAdmin: false,
			
		},
		{
			firstName : "Marcus",
			lastName : "Adoro",
			email: "marcus.ave@gmail.com",
			password: "rockstarmarcus",
			isAdmin: false,
			
		},
		{
			firstName : "Budy",
			lastName : "zabala",
			email: "zabudz@gmail.com",
			password: "basssupremacy",
			isAdmin: false,
		},
		{
			firstName : "Kris",
			lastName : "Dancel",
			email: "dance.kris@gmail.com",
			password: "superproxy",
			isAdmin: false,
			
		}
	])


	>> Add 3 new courses in a new courses collection with the following fields:

		-name - string
		-price - number
		-isActive - boolean 
			note: initially all courses should be inactive

	db.newCourses.insertMany([
				{
				course: "Music theory",
				price: 1500,
				isActive: false
		 		},
				{
				course: "Music Therapy",
				price: 2500,
				isActive: false
				},
				{
				course: "Ethnomusicology",
				price: 2500,
				isActive: false
				}
			])


	>> Note: If the collection you are trying to add a document to does not exist yet, mongodb will add it for you. 

//Read

	>> Find all regular/non-admin users.

	db.users.find({isAdmin: false})

//Update

	>> Update the first user in the users collection as an admin.
	>> Update one of the courses as active.

	db.users.updateOne({}, {$set: {isAdmin: true}})
	db.newCourses.updateOne({}, {$set: {isActive: true}})

//Delete

	>> Delete all inactive courses
	
	db.newCourses.deleteMany({isActive: false})

//Add all of your query/commands here in activity.js












*/